<?php

namespace Drupal\taxonomy_breadcrumb;

use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\VocabularyForm as VocabularyFormBuilderBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class VocabularyListBuilder.
 *
 * @package Drupal\taxonomy_breadcrumb.
 */
class VocabularyForm extends VocabularyFormBuilderBase {

  /**
   * The vocabulary storage.
   *
   * @var \Drupal\taxonomy\VocabularyStorageInterface
   */
  protected $vocabularyStorage;

  /**
   * Constructs a new vocabulary form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->vocabularyStorage = $entity_type_manager->getStorage('taxonomy_vocabulary');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Override Drupal\Core\Config\Entity\ConfigEntityListBuilder::load().
   */
  public function form(array $form, FormStateInterface $form_state) {
    $vocabulary = $this->entity;

    $form['third_party_settings']['taxonomy_breadcrumb_path'] = [
      '#type' => 'textfield',
      '#title' => t('Breadcrumb path (taxonomy_breadcrumb)'),
      '#default_value' => $vocabulary->getThirdPartySetting('taxonomy_breadcrumb', 'taxonomy_breadcrumb_path'),
      '#maxlength' => 128,
      '#description' => t("Specify the path this vocabulary links to as a breadcrumb. If blank, the breadcrumb will not appear. Use a relative path and don't add a trailing slash. For example: node/42 or my/path/alias."),
    ];

    $form = parent::form($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $vocabulary = $this->entity;
    $vocabulary->setThirdPartySetting('taxonomy_breadcrumb', 'taxonomy_breadcrumb_path', $form_state->getValue('taxonomy_breadcrumb_path'));
    parent::save($form, $form_state);
  }

}
